export interface UserPayloadInterface {
    email: string;
    password: string;
}

export interface INotice {
    title: string
    text: string
    // img: string
}

interface INoticePage {
    id: number
    title: string
    text: string
    img: string
    dateCreate: string
    totalCount: number
    totalPage: number
    currentPage: number
}
