import {defineStore} from 'pinia';
import type {INotice} from "~/interface";


// board 스토어
export const useNoticeStore = defineStore('board', {
    state: () => ({
        boards:[],
    }),
    actions: {
        setBoard(data: INotice) {
            useFetch(`http://192.168.0.206:8080/v1/board/new`, {
                method: 'post',
                body: data
            })
        },
        async fetchNotice(id:number) {
            const token = useCookie('token');

            const {data}:any = await useFetch(
                `http://192.168.0.206:8080/v1/board/detail/${id}`, {
                    method: 'GET',
                    headers: {
                        'Authorization': `Bearer ${token}`
                    },
                },
            );
            if (data.value) {
                this.boards = data.value;
            }
        }
    }
})

if (import.meta.hot) {  //HMR
    import.meta.hot.accept(acceptHMRUpdate(useNoticeStore, import.meta.hot))
}
