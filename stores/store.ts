interface IStoreCreate {
    storeName: string
    addressStoreDo: string
    addressStoreG: string
    storeNumber: string
    sellTime: string
    holiday: string
    deliveryArea: string
    bossName: string
    shopId: string
    storeX: number
    storeY: number
}
// auth 스토어
export const useStoreStore = defineStore('auth', {
    state: () => {
        return {
        }
    },
    getters: {},
    actions: {
        setStore(data: IStoreCreate) {
            $fetch(`http://192.168.0.206:8080/v1/store/new`,{
              method: 'POST',
              body: data
            })
        }
    }
})

if (import.meta.hot) {  //HMR
    import.meta.hot.accept(acceptHMRUpdate(useNoticeStore, import.meta.hot))
}
